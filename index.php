<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Twilio Chat</title>
</head>
<body>
<div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel-heading">
                <h1 class="panel-title text-center">Admin mode</h1>
                </div>
                <div class="panel-body">
                    <form method="POST" action="inc\actionlogin.php">
                        <div class="form-group"  >
                        <label for="nombre">Username</label>
                        <input type="text" class="form-control" name="user_name" id="nombre" required placeholder="Username">
                        </div>  
                        <div class="form-group">
                            <label for="pass">Password</label>
                            <input type="password" class="form-control" name="pass" id="pass"  required placeholder="Password">
                        </div>
                        <div class="form-group text-center">
                            <input name="" id="" class="btn btn-dark" type="submit" value="Sign in"  >
                        </div> 
                    </form>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-4 col-md-offset-4">
            <div class="panel-heading">
                <h1 class="panel-title text-center">User mode</h1>
                </div>
                <div class="panel-body">
                    <form method="POST" action="inc\startuser.php">
                        <div class="form-group"  >
                        <label for="nombre">Username</label>
                        <input type="text" class="form-control" name="user_name" id="nombre" required placeholder="Username">
                        </div>  
                        <div class="form-group">
                            <label for="pass">ID</label>
                            <input type="password" class="form-control" name="id" id="id"  required placeholder="ID">
                        </div>
                        <div class="form-group text-center">
                            <input name="" id="" class="btn btn-dark" type="submit" value="Enter"  >
                        </div> 
                    </form>
                </div>
            </div>
        </div>
</div>
</body>
</html>