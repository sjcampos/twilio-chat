<?php

session_start();


//echo $_SESSION['id'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <title>User page</title>
</head>
<body>
<div class="container-fluid">
  <div class="row mt-3">
    <div class="col-6 mt-5">
    <table class ="table table-striped">
            <thead>
                <th>Channels</th>
            </thead>
            <tbody>
            <?php  
                /**This part of the code
                 * charges all the publics channels
                 * and shows them so the user
                 * can choose
                 */
                require_once 'C:/xampp/htdocs/twilio-chat/twilio-php-master/Twilio/autoload.php';
                use Twilio\Rest\Client;

                
                $sid    = "ACa9b0a6fac65f05b1e74142476d66e5db";
                $token  = "7f87bd5ef069ec0cb2210c269b212797";
                $twilio = new Client($sid, $token);
                $channels = $twilio->chat->v2->services("IS6c347eca89384b43a92068878c702997")
                                            ->channels
                                            ->read();

                foreach ($channels as $record) {
                    $name = $record->friendlyName;
                    $id = $record->sid;
                
                ?>
                <tr>
                <form action='\twilio-chat\inc\selectchannel.php' method='POST'>
                <input style='display:none' type='text' class='form-control' name='idc' id='idc' value=<?php echo $id?> >
                <td> <?php echo $name; ?></td>
                <td>
                    <input name="" id="" class="btn btn-dark" type="submit" value="Join"  >
                </td>
                </form>
                </tr>
            <?php } ?>
            </tbody>
            </table>
    </div>
   
    <?php
    /**Checks if the user is in a channel
     * and if the url contents an id channel
     * it proceeds to bring all the message and charge 
     * the rest of components
     */
    if(isset($_GET['channel'])){
        $idchannel = $_GET['channel'];
        $_SESSION['idchannel'] = $idchannel;
        require_once 'C:/xampp/htdocs/twilio-chat/twilio-php-master/Twilio/autoload.php';
        
        $sid    = "ACa9b0a6fac65f05b1e74142476d66e5db";
        $token  = "7f87bd5ef069ec0cb2210c269b212797";
        $twilio = new Client($sid, $token);

            $members = $twilio->chat->v2->services("IS6c347eca89384b43a92068878c702997")
                                        ->channels($idchannel)
                                        ->members
                                        ->read();
            echo "<div class = 'col-6 mt-5'>";
            echo "<th>Members</th>";
            foreach ($members as $record) {
                //echo "<td class='text-left' style='width:490px;'>{$record->sid}</td>";
                //Shows all the members in the channel
                echo "<p>",$record->sid,"</p>";
            }
            echo "</div>";
                
            echo "<div class = 'col m-5'>";
            echo "<th>Chat</th>";
            //Here goes all the message that are charge by ajax
            echo "<div id='msg' class = 'col-md-3 m-5'>";
                
            echo "</div>";
            echo "</div>";

            echo "<div class = 'col m-5'>";
            //Then this part allows the user to send messages to the channel
            echo "<form action='/twilio-chat/inc/sendmessage.php' method='POST'>";
            echo "<th>Message</th>";
            echo "<input  type='text' class='form-control' name='txtmsg' id='txtmsg'>";
            //Save the id of the channel to send the messages
            echo "<input style='display:none' type='text' class='form-control' name='idc' id='idc' value={$idchannel}>";
            echo "<button type='submit' class='btn btn-primary btn-sm' >Send</button>";
            echo "</form>";
            echo "</div>";
        }
    ?>

  </div>
</div>
</body>

<script type="text/javascript">

        $(document).ready(function(){
            /**Sets an interval to call a function
            that brings all the messages in the chat */
        setInterval(function(){
        updatechat();
        }, 3000);
        /**This function communicates with a php file that brings all the messages
        in the record and then shows it in a div */
        function updatechat()
        {
        $.ajax({
        url:"/twilio-chat/inc/reloadchat.php",
        method:"POST",
        success:function(data){
            $('#msg').html(data);
        }
        })
        }

        });  
</script>
</html>