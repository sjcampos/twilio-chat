<?php
    $idc = $_REQUEST['idc'];
    session_start();
    //Saves the id of the channel that is going to be modify
    $_SESSION['updchannel'] = $idc;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Update Channel</title>
</head>
<body>
<div class="container m-5">
      <div class="row justify-content-md-center">
        <div class="col-md-4 col-md-offset-4">
          <div class="login-panel panel panel-default">
            <div class="panel-heading">
              <h1 class="panel-title text-center">Change name</h1>
            </div>
            <div class="panel-body">
              <form  method="POST" action="\twilio-chat\inc\updatechannel.php" >
          
               <div class="form-group"  >
                <label for="nombre">New name</label>
                <input type="text" class="form-control" name="newname" id="nombre" required placeholder="New name">
              </div>  
              <div class="form-group text-center">
                <input name="" id="" class="btn btn-dark" type="submit" value="Save"  >
              </div> 
              </form>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div> 
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</html>