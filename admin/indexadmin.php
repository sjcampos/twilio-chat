<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Admin page</title>
</head>
<body>
<div class="container">
    <h1>Channels</h1>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">List</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">New channel</a>
    </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <table class ="table table-striped">
            <thead>
                <th>Channel </th>
                <th>Delete</th>
                <th>Update</th>
            </thead>
            <tbody>
            <?php  
            //Gets all the channels that were created
            //and shows them in a table with the delete and update function 
                require_once 'C:/xampp/htdocs/twilio-chat/twilio-php-master/Twilio/autoload.php';
                use Twilio\Rest\Client;
                $sid    = "ACa9b0a6fac65f05b1e74142476d66e5db";
                $token  = "7f87bd5ef069ec0cb2210c269b212797";
                $twilio = new Client($sid, $token);
                $channels = $twilio->chat->v2->services("IS6c347eca89384b43a92068878c702997")
                                            ->channels
                                            ->read();

                foreach ($channels as $record) {
                    $name = $record->friendlyName;
                    $id = $record->sid;
                
                ?>
                <tr>
                <form action='\twilio-chat\inc\deletechannel.php' method='POST'>
                <input style='display:none' type='text' class='form-control' name='idc' id='idc' value=<?php echo $id?> >
                <td> <?php echo $name; ?></td>
                <td>
                    <input name="" id="" class="btn btn-dark" type="submit" value="Delete" >
                </td>
                </form>
                <form action='\twilio-chat\update.php' method='POST'>
                <input style='display:none' type='text' class='form-control' name='idc' id='idc' value=<?php echo $id?> >
                <td>
                <input name="" id="" class="btn btn-dark" type="submit" value="Update" >                        
                </td>
                 </form>
                </tr>
            <?php } ?>
            </tbody>
            </table>
        </div>
        <!-- Shows a form to create a new channel-->
        <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                 <div class="container">
                            <div class="row justify-content-md-center">
                                <div class="col-md-4 col-md-offset-4">
                                    <div class="login-panel panel panel-default">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-center">New Channel</h1>
                                            <div class="panel-body">
                                                <form  method="POST" action="\twilio-chat\inc\addchannels.php">
                                                <div class="form-group"  >
                                                    <label for="name">Write a friendly name</label>
                                                        <input type="text" class="form-control" name="txtname" id="name" required placeholder="Friendly name">
                                                    </div> 
                                                    <div class="form-group text-center">
                                                        <input name="" id="" class="btn btn-dark" type="submit" value="Save"  >
                                                    </div> 
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </div>
        </div>
    </div>
</body>
</html>